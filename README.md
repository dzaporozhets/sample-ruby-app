# Sample ruby application to test GitLab CI/CD features


### Build and run locally

```
docker build -t sample-ruby-app .
docker run -p 5000:5000 sample-ruby-app
```

### Pull master from container registry

```
docker run  -p 5000:5000 registry.gitlab.com/dzaporozhets/sample-ruby-app:master
```

### How to manuall deploy to Google Cloud: 

1. Create cluster under containter engine
1. Connect to cluster
1. Deploy docker image

    ```
    kubectl run sample-ruby-app --image=registry.gitlab.com/dzaporozhets/sample-ruby-app:master --port=5000
    kubectl expose deployment sample-ruby-app --type="LoadBalancer"
    kubectl get services
    ```
1. Visit ip address from laster command on port 5000
